import pytest
from selenium import webdriver
from selenium.webdriver.edge.options import Options
import allure


@allure.feature('browser_run')
@pytest.fixture()
def browser():
    options = Options()
    with allure.step('headless'):
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        chrome_browser = webdriver.Chrome(options=options)
        chrome_browser.implicitly_wait(10)
        return chrome_browser
